package net.myswimrecord.beforeandafter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.myswimrecord.beforeandafter.model.PictureViewModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static android.os.Environment.DIRECTORY_PICTURES;
import static android.os.Environment.getExternalStorageDirectory;
import static android.os.Environment.getExternalStoragePublicDirectory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavController.OnDestinationChangedListener {

    private static final int REQUEST_STORAGE_PERMISSION = 0x1172;
    private static final int REQUEST_STORAGE_PERMISSION_RETRY = 0x4FDE;

    private static int currentRequestStoragePermission;

    private static final String FRAGMENT_DIALOG = "dialog";
    private static final String SAVE_FILE_TEMPLATE = "BeforeNAfter_%d.jpg";

    Activity mActivity;

    Fragment mFragment1;
    Fragment mFragment2;

    NavController mNavController1;
    NavController mNavController2;

    HashMap<String,Boolean> mSavableHashMap = new HashMap<>();

    Button mBtnSave;

    Bitmap mCombinedImage;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {}
    }

    @Override
    public void onBackPressed() {
        if (mNavController1.getCurrentDestination().getId() != R.id.step03
            && mNavController2.getCurrentDestination().getId() != R.id.step03) {
            new ExitDialog().show(getSupportFragmentManager(), null);
        } else {
            if (mNavController1.getCurrentDestination().getId() == R.id.step03)
                mNavController1.popBackStack();
            if (mNavController2.getCurrentDestination().getId() == R.id.step03)
                mNavController2.popBackStack();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;

        mNavController1 = Navigation.findNavController(this, R.id.my_nav_host_fragment1);
        mNavController2 = Navigation.findNavController(this, R.id.my_nav_host_fragment2);

        mFragment1 = getSupportFragmentManager().findFragmentById(R.id.my_nav_host_fragment1);
        mFragment2 = getSupportFragmentManager().findFragmentById(R.id.my_nav_host_fragment2);
        PictureViewModel mPVM1 = new ViewModelProvider(mFragment1).get(PictureViewModel.class);
        PictureViewModel mPVM2 = new ViewModelProvider(mFragment2).get(PictureViewModel.class);

        mNavController1.addOnDestinationChangedListener(this);
        mNavController2.addOnDestinationChangedListener(this);

        mSavableHashMap.put(mFragment1.getTag(),false);
        mSavableHashMap.put(mFragment2.getTag(),false);

        mBtnSave = findViewById(R.id.btnSave);
        mBtnSave.setOnClickListener(this);

        mPVM1.getBitmap().observe(mFragment1.getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                mSavableHashMap.put(mFragment1.getTag(),true);
                TextView tvWhen = mFragment1.getView().findViewById(R.id.tvWhen);
                tvWhen.setText("전");
                checkIfSavable();
            }
        });
        mPVM2.getBitmap().observe(mFragment2.getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                mSavableHashMap.put(mFragment2.getTag(),true);
                TextView tvWhen = mFragment2.getView().findViewById(R.id.tvWhen);
                tvWhen.setText("후");
                checkIfSavable();
            }
        });

        currentRequestStoragePermission = REQUEST_STORAGE_PERMISSION;
    }

    private void checkIfSavable() {
        if (mBtnSave == null)
            return;
        if (mSavableHashMap.containsValue(false))
            mBtnSave.setEnabled(false);
        else
            mBtnSave.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        if (v == mBtnSave) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestStoragePermission();
                return;
            }
            save();
        }
    }

    private void save() {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                ImageView imageView1 = mFragment1.getView().findViewById(R.id.imageView);
                Bitmap bitmap1 = Bitmap.createBitmap(imageView1.getWidth(), imageView1.getHeight(), Bitmap.Config.ARGB_8888);
                TextView tvWhen1 = mFragment1.getView().findViewById(R.id.tvWhen);
                tvWhen1.buildDrawingCache();
                Bitmap bitmap1_when = tvWhen1.getDrawingCache();
                Canvas canvas1 = new Canvas(bitmap1);
                imageView1.draw(canvas1);
                canvas1.drawBitmap(bitmap1_when, bitmap1.getWidth()/2 - bitmap1_when.getWidth()/2, bitmap1.getHeight() - bitmap1_when.getHeight(), null);

                ImageView imageView2 = mFragment2.getView().findViewById(R.id.imageView);
                Bitmap bitmap2 = Bitmap.createBitmap(imageView2.getWidth(), imageView2.getHeight(), Bitmap.Config.ARGB_8888);
                TextView tvWhen2 = mFragment2.getView().findViewById(R.id.tvWhen);
                tvWhen2.buildDrawingCache();
                Bitmap bitmap2_when = tvWhen2.getDrawingCache();
                Canvas canvas2 = new Canvas(bitmap2);
                imageView2.draw(canvas2);
                canvas2.drawBitmap(bitmap2_when, bitmap2.getWidth()/2 - bitmap2_when.getWidth()/2, bitmap2.getHeight() - bitmap2_when.getHeight(), null);

                mCombinedImage = Utils.intoSingleImage(new ArrayList<>(Arrays.asList(bitmap1,bitmap2)), true, true);

                int fileIndex = 0;
                String saveFileName = null;
                OutputStream outputStream = null;

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    File file;
                    File dir = new File(getExternalStoragePublicDirectory(DIRECTORY_PICTURES), "TestTest");
                    if (!dir.exists()) {
                        try {
                            if (!dir.mkdirs()) {
                                return;
                            }
                        } catch (SecurityException e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                    while (true) {
                        saveFileName = String.format(SAVE_FILE_TEMPLATE,fileIndex++);
                        file = new File(dir, saveFileName);
                        if (!file.exists())
                            break;
                    }
                    File saveFile = new File(dir,saveFileName);
                    try {
                        saveFile.createNewFile();
                        outputStream = new FileOutputStream(saveFile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ContentResolver contentResolver = getContentResolver();
                    Uri contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    Cursor cursor;
                    String[] projection = new String[] { MediaStore.MediaColumns.DISPLAY_NAME };
                    while (true) {
                        saveFileName = String.format(SAVE_FILE_TEMPLATE,fileIndex++);
                        cursor = contentResolver.query(contentUri, projection,MediaStore.MediaColumns.DISPLAY_NAME + " = ?",
                                new String[] {saveFileName},null);
                        if (cursor == null || !cursor.moveToFirst())
                            break;
                    }
                    Uri uri = null;
                    try {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, saveFileName);
                        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/*");
                        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, DIRECTORY_PICTURES+"/TestTest");
                        uri = contentResolver.insert(contentUri, contentValues);
                        if (uri == null)
                            throw new IOException("Failed to create new MediaStore record.");
                        outputStream = contentResolver.openOutputStream(uri);
                    } catch (IOException e) {
                        e.printStackTrace();
                        if (uri != null)
                            contentResolver.delete(uri, null, null);
                    }
                }

                try {
                    if (outputStream != null) {
                        if (mCombinedImage.compress(Bitmap.CompressFormat.JPEG, 95, outputStream) == false)
                            throw new IOException("Failed to save results.");
                        else {
                            final String successMessage = String.format(getString(R.string.save_success), saveFileName);
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), successMessage, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } else {
                        throw new IOException("Failed to get output stream.");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null)
                            outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (currentRequestStoragePermission == REQUEST_STORAGE_PERMISSION)
                    new ConfirmationDialog().show(getSupportFragmentManager(), FRAGMENT_DIALOG);
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            if (requestCode == REQUEST_STORAGE_PERMISSION_RETRY) {
                ErrorDialog.newInstance(getString(R.string.request_storage_permission_failed))
                        .show(getSupportFragmentManager(), FRAGMENT_DIALOG);
            } else if (requestCode == REQUEST_STORAGE_PERMISSION) {
                requestStoragePermission();
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } else {
            if (requestCode == REQUEST_STORAGE_PERMISSION || requestCode == REQUEST_STORAGE_PERMISSION_RETRY)
                save();
        }
    }

    @Override
    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
        if (controller == mNavController1) {
            mSavableHashMap.put(mFragment1.getTag(), destination.getId() == R.id.step03 ? true : false);
        } else if (controller == mNavController2) {
            mSavableHashMap.put(mFragment2.getTag(), destination.getId() == R.id.step03 ? true : false);
        }
        checkIfSavable();
    }

    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            currentRequestStoragePermission = REQUEST_STORAGE_PERMISSION;
                        }
                    })
                    .create();
        }
    }

    public static class ConfirmationDialog extends DialogFragment {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_storage_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                currentRequestStoragePermission = REQUEST_STORAGE_PERMISSION_RETRY;
                                activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        REQUEST_STORAGE_PERMISSION_RETRY);
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,null)
                    .create();
        }
    }

    public static class ExitDialog extends DialogFragment {
        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(R.string.confirm_exit)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.finish();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .create();
        }
    }
}
