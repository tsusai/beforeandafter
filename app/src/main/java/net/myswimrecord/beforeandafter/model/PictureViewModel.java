package net.myswimrecord.beforeandafter.model;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PictureViewModel extends ViewModel {

    public static final int RESULT_IMAGE_FROM_CAMERA = 0x7777;
    public static final int RESULT_IMAGE_FROM_ALBUM = 0x04F7;

    private MutableLiveData<Bitmap> mBitmap = new MutableLiveData<>();

    public void setBitmap(Bitmap bitmap) {
        mBitmap.setValue(bitmap);
    }

    public LiveData<Bitmap> getBitmap() {
        return mBitmap;
    }


}
