package net.myswimrecord.beforeandafter.fragment;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import net.myswimrecord.beforeandafter.R;
import net.myswimrecord.beforeandafter.model.PictureViewModel;

public class Step03 extends Fragment {

    PictureViewModel mPictureViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.step03, container, false);

        final ImageView imageView = view.findViewById(R.id.imageView);
        TextView tvWhen = view.findViewById(R.id.tvWhen);
        tvWhen.setDrawingCacheEnabled(true);

        Fragment parent = this.getParentFragment();
        mPictureViewModel = new ViewModelProvider(parent != null ? parent : this.getActivity()).get(PictureViewModel.class);
        mPictureViewModel.getBitmap().observe(this.getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                imageView.setImageBitmap(bitmap);
            }
        });

        return view;
    }
}
