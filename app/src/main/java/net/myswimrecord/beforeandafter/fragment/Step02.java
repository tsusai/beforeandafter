package net.myswimrecord.beforeandafter.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.text.TextUtilsCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import net.myswimrecord.beforeandafter.R;
import net.myswimrecord.beforeandafter.Utils;
import net.myswimrecord.beforeandafter.camera.CameraActivity;
import net.myswimrecord.beforeandafter.model.PictureViewModel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;
import static net.myswimrecord.beforeandafter.model.PictureViewModel.RESULT_IMAGE_FROM_ALBUM;
import static net.myswimrecord.beforeandafter.model.PictureViewModel.RESULT_IMAGE_FROM_CAMERA;

public class Step02 extends Fragment {

    PictureViewModel mPictureViewModel;
    View mFragmentView;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            switch(requestCode) {

                case RESULT_IMAGE_FROM_CAMERA:

                    String imagePath = data.getStringExtra(CameraActivity.IMAGE_PATH);
                    if (TextUtils.isEmpty(imagePath))
                        return;
                    Log.d("Step02", "onActivityResult called. imagePath is " + imagePath);

                    Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                    ExifInterface exif = null;
                    try {
                        exif = new ExifInterface(imagePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (exif != null) {
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        if (orientation != -1) {
                            bitmap = Utils.rotateBitmap(bitmap, orientation);
                        }
                    }
                    toStep03(bitmap);
                    break;

                case RESULT_IMAGE_FROM_ALBUM:

                    Uri imageUri = data.getData();
                    if (imageUri == null)
                        return;
                    Log.d("Step02", "onActivityResult called. imageUri is " + imageUri.toString());
                    try {
                        InputStream inputStream = getActivity().getContentResolver().openInputStream(imageUri);
                        toStep03(BitmapFactory.decodeStream(inputStream));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private void toStep03(Bitmap bitmap) {
        mPictureViewModel.setBitmap(bitmap);
        Navigation.findNavController(mFragmentView).navigate(R.id.action_step02_to_step03);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mFragmentView = inflater.inflate(R.layout.step02, container, false);

        mFragmentView.findViewById(R.id.btnTakePicture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromCamera();
            }
        });

        mFragmentView.findViewById(R.id.btnChoosePicture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromAlbum();
            }
        });

        Fragment parent = this.getParentFragment();
        mPictureViewModel = new ViewModelProvider(parent != null ? parent : this.getActivity()).get(PictureViewModel.class);

        return mFragmentView;
    }

    private void getImageFromCamera() {

        Intent intent = new Intent(getContext(), CameraActivity.class);
        startActivityForResult(intent, RESULT_IMAGE_FROM_CAMERA);
    }

    private void getImageFromAlbum() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, RESULT_IMAGE_FROM_ALBUM);
    }
}
