package net.myswimrecord.beforeandafter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import net.myswimrecord.beforeandafter.R;
import net.myswimrecord.beforeandafter.model.PictureViewModel;

public class Step01 extends Fragment {

    PictureViewModel mPictureViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.step01, container, false);

        view.findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_step01_to_step02);
            }
        });

        Fragment parent = this.getParentFragment();
        mPictureViewModel = new ViewModelProvider(parent != null ? parent : this.getActivity()).get(PictureViewModel.class);

        return view;
    }
}
