package net.myswimrecord.beforeandafter;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.text.Layout;

import java.util.ArrayList;

public class Utils {

    public static Bitmap intoSingleImage (ArrayList<Bitmap> bitmaps, boolean horizontal, boolean alignCenter) {
        int width = 0;
        int height = 0;
        for (int i = 0 ; i < bitmaps.size() ; i++) {
            height = horizontal
                    ? (bitmaps.get(i).getHeight() > height ? bitmaps.get(i).getHeight() : height)
                    : height + bitmaps.get(i).getHeight();
            width = horizontal
                    ? width + bitmaps.get(i).getWidth()
                    : (bitmaps.get(i).getWidth() > width ? bitmaps.get(i).getWidth() : width);
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        int start = 0;
        int top = 0;
        for (int i = 0 ; i < bitmaps.size() ; i++) {
            start = horizontal
                    ? (i == 0 ? 0 : start + bitmaps.get(i-1).getWidth())
                    : (alignCenter ? (width - bitmaps.get(i).getWidth()) / 2 : 0);
            top = horizontal
                    ? (alignCenter ? (height - bitmaps.get(i).getHeight()) / 2 : 0)
                    : (i == 0 ? 0 : top + bitmaps.get(i-1).getHeight());
            canvas.drawBitmap(bitmaps.get(i), start, top, null);
        }
        return bitmap;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                return bitmap;
        }
        Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return bmRotated;
    }
}
